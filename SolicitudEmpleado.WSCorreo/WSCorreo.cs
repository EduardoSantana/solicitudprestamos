﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using SolicitudEmpleado.WSCorreo.WSIntranetCorreo;
using System.Net.Mail;

namespace SolicitudEmpleado.WSCorreo
{
    public static class clWSCorreo
    {
        public static bool NotificacionCorreo(string destinatario, string titulo, string encabezado, string subtitulo,
                                              string cuerpo, string usuario, string ip)
        {
            var webService = new MailService();
            var request = new MailPlantillaRequest();
            var info = new ServiceRequestInfo();

            info.Aplicacion = WebConfigurationManager.AppSettings["Seguridad.App.Nombre"];
            info.Usuario = usuario;
            info.IP = ip;

            request.Info = info;
            request.Titulo = titulo;
            request.Encabezado = encabezado;
            request.Subtitulo = subtitulo;
            request.Cuerpo = cuerpo;
            request.Destinatario = destinatario;
            request.Remitente = WebConfigurationManager.AppSettings["Mail.Remitente"];
            request.Plantilla = TipoPlantillaCorreo.Intranet2013;

            var response = webService.EnviarCorreoPlantilla(request);
            return response.Info.OK;
        }

        public static bool NotificacionCorreo(MailMessage message, string encabezado, string subtitulo)
        {
            var webService = new MailService();
            var request = new MailPlantillaRequest();
            var info = new ServiceRequestInfo();

            info.Aplicacion = WebConfigurationManager.AppSettings["Seguridad.App.Nombre"];
            info.Usuario = "Anonimo";
            info.IP = "0.0.0.0";

            request.Info = info;
            request.Titulo = message.Subject;
            request.Cuerpo = message.Body;
            request.Encabezado = encabezado;
            request.Subtitulo = subtitulo;
            request.Destinatario = message.To.ToString();
            request.Remitente = WebConfigurationManager.AppSettings["Mail.Remitente"];
            request.Plantilla = TipoPlantillaCorreo.Intranet2013;

            var response = webService.EnviarCorreoPlantilla(request);
            return response.Info.OK;
        }
   }
}