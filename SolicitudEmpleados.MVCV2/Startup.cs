﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolicitudEmpleados.MVCV2.Startup))]
namespace SolicitudEmpleados.MVCV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
