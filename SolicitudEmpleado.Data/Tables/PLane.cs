namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Planes")]
    public partial class Plane
    {
        public Plane()
        {

        }

        [Key]
        [DisplayName("No")]
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Nombre")]
        public string name { get; set; }

        [Required]
        [DisplayName("Precio")]
        public int precio { get; set; } //  ] NULL,
        [Required]
        [DisplayName("Minutos")]
        public int minutos { get; set; } //  ] NULL,
        [Required]
        public decimal OnNet { get; set; } //  ](18, 2) NULL,
        [Required]
        public decimal OffNet { get; set; } //  ](18, 2) NULL,
        [Required]
        [DisplayName("Active")]
        public bool active { get; set; } //  ] NULL,
        [Required]
        public decimal OnNetBilled { get; set; } //  (18,2),
        [Required]
        public decimal OffNetBilled { get; set; } //  (18,2),
        public int CreadoPor { get; set; } //  ] NOT NULL,
        [DisplayName("Fecha Creacion")]
        public DateTime FechaCreacion { get; set; } //  ] NOT NULL,
        public int? ModificadoPor { get; set; } //  ] NULL,
        [DisplayName("Fecha Modifiacion")]
        public DateTime? FechaModificacion { get; set; } //  ] NULL,
    }
}
