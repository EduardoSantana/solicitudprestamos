﻿using AutenticacionData.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{

	public class LoginViewModel
	{
		[Required]
		[Display(Name = "User name")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[Display(Name = "Remember me?")]
		public bool RememberMe { get; set; }

		public bool IsValid()
		{
			var retVal = clValidarUsuario.IsValid(this.UserName, Encode(this.Password));
			return retVal;
			//using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ModelFollowTaskIt"].ConnectionString))
			//{
			//	string _sql = @"SELECT [usuario] FROM [dbo].[EMPLEADOS] WHERE [usuario] = @u AND [PasswordHash] = @p";
			//	var cmd = new SqlCommand(_sql, cn);
			//	cmd.Parameters
			//		.Add(new SqlParameter("@u", SqlDbType.NVarChar))
			//		.Value = this.UserName;
			//	cmd.Parameters
			//		.Add(new SqlParameter("@p", SqlDbType.NVarChar))
			//		.Value = Encode(this.Password);
			//	cn.Open();
			//	var reader = cmd.ExecuteReader();
			//	if (reader.HasRows)
			//	{
			//		reader.Dispose();
			//		cmd.Dispose();
			//		return true;
			//	}
			//	else
			//	{
			//		reader.Dispose();
			//		cmd.Dispose();
			//		return false;
			//	}
			//}
		}

		public static string Encode(string value)
		{
			var hash = System.Security.Cryptography.SHA1.Create();
			var encoder = new System.Text.ASCIIEncoding();
			var combined = encoder.GetBytes(value ?? "");
			return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
		}

	}

	

}
