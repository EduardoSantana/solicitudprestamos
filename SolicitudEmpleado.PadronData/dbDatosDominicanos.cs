namespace SolicitudEmpleado.PadronData
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;
	using System.Collections.Generic;
	using System.Data.SqlClient;

	public partial class dbDatosDominicanos : DbContext
	{
		public dbDatosDominicanos()
			: base("name=dbDatosDominicanos")
		{
		}

		public virtual DbSet<CEDULAS> CEDULAS { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.MUN_CED)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SEQ_CED)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.VER_CED)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CED_A_NUM)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CED_A_SERI)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SEXO_ANTERIOR)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NOMBRES)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.APELLIDOS)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.FECHA_NAC)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.LUGAR_NAC)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SEXO_ACTUAL)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_SEXO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_SANGRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_SANGRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_PIEL)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_PIEL)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.ESTADO_CIVIL_NUEVO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_ESTADO_CIVIL_NUEVO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SENAS_PARTICULARES)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_OCUP)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.OCUPACION)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.PROVINCIA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_MUNICIPIO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_MUNICIPIO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CODIGO_CIUDAD)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_CIUDAD)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CODIGO_SECTOR)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SECTOR)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CALLE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NUMERO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.EDIFICIO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.PISO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.APARTAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.TELEFONO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NOMBRE_PADRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CEDULA_PADRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NOMBRE_MADRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CEDULA_MADRE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NOMBRE_CONYUGUE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.CED_CONYUGUE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_NACION)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_NACIONALIDAD)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NO_PASAPORTE)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.NO_RESIDENCIA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_CATEGORIA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_CATEGORIA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.TIPO_CAUSA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_TIPO_CAUSA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.COD_CAUSA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DESC_CAUSA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.STATUS)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.DIRECTORIO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.ARCHIVO)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.SECUENCIA)
				.IsUnicode(false);

			modelBuilder.Entity<CEDULAS>()
				.Property(e => e.USUARIO_CREO)
				.IsUnicode(false);
		}

		public List<CEDULAS> ConsultaCedula(decimal? cedula = null, decimal? cedula_anterior = null, string nombre = null, string apellido = null)
		{
			List<CEDULAS> cedulas;

			SqlParameter cedulaParam = new SqlParameter("@cedula", cedula);
			if (cedula == null)
			{
				cedulaParam.Value = DBNull.Value;
			}

			SqlParameter cedulaAnteriorParam = new SqlParameter("@cedulaAnterior", cedula_anterior);
			if (cedula_anterior == null)
			{
				cedulaAnteriorParam.Value = DBNull.Value;
			}

			SqlParameter nombreParam = new SqlParameter("@nombre", nombre);
			if (nombre == null)
			{
				nombreParam.Value = DBNull.Value;
			}

			SqlParameter apellidoParam = new SqlParameter("@apellido", apellido);
			if (apellido == null)
			{
				apellidoParam.Value = DBNull.Value;
			}

			cedulas = this.Database.SqlQuery<CEDULAS>("[dbo].[sp_Get_DataCedulas] @cedula, @cedulaAnterior, @nombre, @apellido", cedulaParam, cedulaAnteriorParam, nombreParam, apellidoParam).ToList();

			return cedulas;
		}
	
	}
}
