namespace SolicitudEmpleado.PadronData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CEDULAS
    {
		[Key]
		public Decimal CEDULA_NUEVA { get; set; }

        [StringLength(256)]
        public string MUN_CED { get; set; }

        [StringLength(256)]
        public string SEQ_CED { get; set; }

        [StringLength(256)]
        public string VER_CED { get; set; }

		public Decimal? CEDULA_ANTERIOR { get; set; }

        [StringLength(256)]
        public string CED_A_NUM { get; set; }

        [StringLength(256)]
        public string CED_A_SERI { get; set; }

        [StringLength(256)]
        public string SEXO_ANTERIOR { get; set; }

        [StringLength(256)]
        public string NOMBRES { get; set; }

        [StringLength(256)]
        public string APELLIDOS { get; set; }

        [StringLength(256)]
        public string FECHA_NAC { get; set; }

        [StringLength(256)]
        public string LUGAR_NAC { get; set; }

        [StringLength(256)]
        public string SEXO_ACTUAL { get; set; }

        [StringLength(256)]
        public string DESC_SEXO { get; set; }

        [StringLength(256)]
        public string COD_SANGRE { get; set; }

        [StringLength(256)]
        public string DESC_SANGRE { get; set; }

        [StringLength(256)]
        public string COD_PIEL { get; set; }

        [StringLength(256)]
        public string DESC_PIEL { get; set; }

        [StringLength(256)]
        public string ESTADO_CIVIL_NUEVO { get; set; }

        [StringLength(256)]
        public string DESC_ESTADO_CIVIL_NUEVO { get; set; }

        [StringLength(256)]
        public string SENAS_PARTICULARES { get; set; }

        [StringLength(256)]
        public string COD_OCUP { get; set; }

        [StringLength(256)]
        public string OCUPACION { get; set; }

        [StringLength(256)]
        public string PROVINCIA { get; set; }

        [StringLength(256)]
        public string COD_MUNICIPIO { get; set; }

        [StringLength(256)]
        public string DESC_MUNICIPIO { get; set; }

        [StringLength(256)]
        public string CODIGO_CIUDAD { get; set; }

        [StringLength(256)]
        public string DESC_CIUDAD { get; set; }

        [StringLength(256)]
        public string CODIGO_SECTOR { get; set; }

        [StringLength(256)]
        public string SECTOR { get; set; }

        [StringLength(256)]
        public string CALLE { get; set; }

        [StringLength(256)]
        public string NUMERO { get; set; }

        [StringLength(256)]
        public string EDIFICIO { get; set; }

        [StringLength(256)]
        public string PISO { get; set; }

        [StringLength(256)]
        public string APARTAMENTO { get; set; }

        [StringLength(256)]
        public string TELEFONO { get; set; }

        [StringLength(256)]
        public string NOMBRE_PADRE { get; set; }

        [StringLength(256)]
        public string CEDULA_PADRE { get; set; }

        [StringLength(256)]
        public string NOMBRE_MADRE { get; set; }

        [StringLength(256)]
        public string CEDULA_MADRE { get; set; }

        [StringLength(256)]
        public string NOMBRE_CONYUGUE { get; set; }

        [StringLength(256)]
        public string CED_CONYUGUE { get; set; }

        [StringLength(256)]
        public string COD_NACION { get; set; }

        [StringLength(256)]
        public string DESC_NACIONALIDAD { get; set; }

        [StringLength(256)]
        public string NO_PASAPORTE { get; set; }

        [StringLength(256)]
        public string NO_RESIDENCIA { get; set; }

        public DateTime? FECHA_EXPEDICION { get; set; }

		public DateTime? FECHA_NATURALIZACION { get; set; }

        [StringLength(256)]
        public string COD_CATEGORIA { get; set; }

        [StringLength(256)]
        public string DESC_CATEGORIA { get; set; }

        [StringLength(256)]
        public string TIPO_CAUSA { get; set; }

        [StringLength(256)]
        public string DESC_TIPO_CAUSA { get; set; }

        [StringLength(256)]
        public string COD_CAUSA { get; set; }

        [StringLength(256)]
        public string DESC_CAUSA { get; set; }

        [StringLength(256)]
        public string STATUS { get; set; }

        [StringLength(256)]
        public string DIRECTORIO { get; set; }

        [StringLength(256)]
        public string ARCHIVO { get; set; }

        [StringLength(256)]
        public string SECUENCIA { get; set; }

        public DateTime? FECHA_CREO { get; set; }

        [StringLength(256)]
        public string USUARIO_CREO { get; set; }

		public string DIRECCION { 
			get 
			{
				return (string.IsNullOrEmpty(this.PROVINCIA) ? "" : this.PROVINCIA.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.DESC_MUNICIPIO) ? "" : this.DESC_MUNICIPIO.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.DESC_CIUDAD) ? "" : this.DESC_CIUDAD.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.SECTOR) ? "" : this.SECTOR.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.CALLE) ? "" : this.CALLE.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.DESC_MUNICIPIO) ? "" : this.DESC_MUNICIPIO.ToString()) + ",  No: " +
						(string.IsNullOrEmpty(this.NUMERO) ? "" : this.NUMERO.ToString()) + ",  " +
						(string.IsNullOrEmpty(this.EDIFICIO) ? "" : this.EDIFICIO.ToString());
			} 
		}

    }
}
