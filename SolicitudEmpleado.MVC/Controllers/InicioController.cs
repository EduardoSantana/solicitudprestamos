﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeguridadComun;
using System.Security.Principal;

namespace SolicitudEmpleado.MVC.Controllers
{
	//[Authorize]
	[AllowAnonymous]
    public class InicioController : BaseSeguridadController
    {
        
        //[ValidarModulo("SolicitudPrestamo")]
	
        public ActionResult Index()
        {
			var asdf = User.Identity.IsAuthenticated;
            return View();
        }

        //
        // GET: /Inicio/Acerca

        public ActionResult Acerca()
        {
            return View();
        }

        
    }
}
