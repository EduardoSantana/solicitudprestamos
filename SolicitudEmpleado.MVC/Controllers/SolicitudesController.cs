﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeguridadComun;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.DAL;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;
using SolicitudEmpleado.MVC.Componentes.Email;

namespace SolicitudEmpleado.MVC.Controllers
{
    /// <summary>
    /// Esta es la clase principal de todo el programa.
    /// </summary>
    public class SolicitudesController : BaseController
    {
        #region Procesar Resultados de Consultas a Solicitudes

        private List<int> listaEstado = new List<int>();

        private List<int> listaTipos = new List<int>();

        private IEnumerable<object> getResultado(int jtStartIndex, int jtPageSize, string jtSorting, string searchString, ref int intCantidad, int? empresaID = 0, int? estatusID = 0, int? tipoID = 0, int viewType = 0, int asignadoID = 0, string q = "", int qt = 0)
        {
            var solicitudes = (
                from obj in unitOfWork.SolicitudesRepository.Get(includeProperties: "TiposSolicitud")
                select new
                {
                    SolicitudID = obj.SolicitudID,
                    EmpresaID = obj.EmpresaID,
                    TipoSolicitudID = obj.TipoSolicitudID,
                    TipoID = obj.TipoID,
                    EstatusID = obj.EstatusID,
                    FechaSolicitud = obj.FechaSolicitud,
                    CodigoEmpleado = obj.CodigoEmpleado,
                    NombreEmpleado = obj.NombreEmpleado,
                    Direccion = obj.Direccion,
                    TelefonoOficina = obj.TelefonoOficina,
                    Extension = obj.Extension,
                    TelefonoCelular = obj.TelefonoCelular,
                    Email = obj.Email,
                    Posicion = obj.Posicion,
                    Departamento = obj.Departamento,
                    TiempoServicio = obj.TiempoServicio,
                    Cuenta = obj.Cuenta,
                    Sueldo = obj.Sueldo,
                    CreadoPor = obj.CreadoPor,
                    FechaCreacion = obj.FechaCreacion,
                    ModificadoPor = obj.ModificadoPor,
                    FechaModificacion = obj.FechaModificacion,
                    AsignadoA = obj.AsignadoA,
                    NumeroSolicitud = obj.NumeroSolicitud,
                    UsuarioDeRed = obj.UsuarioDeRed,
                    Cedula = obj.Identificacion,
                    AsignadoANombre = obj.AsignadoANombre,
                    Fecha = obj.FechaSolicitud,
                    EmpresaNombre = obj.Empresa.Nombre,
                    Monto = obj.Monto
                });

            if (!String.IsNullOrEmpty(searchString))
            {
                solicitudes = solicitudes.Where(p => p.CodigoEmpleado.ToString().ToUpper().Trim() == searchString.ToUpper() ||
                                                p.Cedula.ToUpper().ToUpper().Trim() == searchString.ToUpper() ||
                                                p.SolicitudID.ToString().ToUpper() == searchString.ToUpper() ||
                                                p.NombreEmpleado.Contains(searchString.ToUpper()));
            } 
     
            if (!String.IsNullOrEmpty(q))
            {
                if (qt == 0)
                {
                    solicitudes = solicitudes.Where(p => p.CodigoEmpleado.ToString() == q);
                }
                if (qt == 1)
                {
                    solicitudes = solicitudes.Where(p => p.Cedula == q);
                }
                if (qt == 2)
                {
                    solicitudes = solicitudes.Where(p => p.SolicitudID.ToString() == q);
                }
                if (qt == 3)
                {
                    solicitudes = solicitudes.Where(p => p.NumeroSolicitud == q);
                }

            }

            if ((estatusID == 0 || estatusID == null) && listaEstado.Count > 0)
            {
                if (viewType == stViewType.Pendientes && (tipoID == null || tipoID == 0) && (empresaID == null || empresaID == 0) && (asignadoID == 0))
                {
                    solicitudes = from p in solicitudes
                                    where p.EstatusID == 1
                                    select p;
                }
                else
                {
                    solicitudes = from p in solicitudes
                                    where listaEstado.Contains(p.EstatusID)
                                    select p;
                }

            }

            if (estatusID > 0)
            {
                solicitudes = from p in solicitudes
                                where p.EstatusID == estatusID
                                select p;

            }

            if ((tipoID == 0 || tipoID == null) && listaTipos.Count > 0)
            {
                solicitudes = from p in solicitudes
                                where listaTipos.Contains(p.TipoID)
                                select p;
            }

            if (tipoID > 0)
            {
                solicitudes = from p in solicitudes
                                where p.TipoID == tipoID
                                select p;
            }
            if (empresaID > 0)
            {
                solicitudes = from p in solicitudes
                                where p.EmpresaID == empresaID
                                select p;
            }
            if (asignadoID > 0)
            {
                solicitudes = solicitudes.Where(p => p.AsignadoA == asignadoID);
            }
      
            if (viewType == stViewType.MisSolicitudes)
            {
                solicitudes = solicitudes.Where(p => p.AsignadoA == currentUser.codigoEmpleado);
            }
          
            if (viewType == stViewType.Pendientes || viewType == stViewType.EnProceso)
            {
                if (viewType == stViewType.Pendientes && asignadoID <= 0)
                {
                    solicitudes = solicitudes.Where(p => p.AsignadoA == null || p.AsignadoA == 0);
                }
            }
            intCantidad = solicitudes.Count();
            return solicitudes.OrderBy(s => s.SolicitudID).Skip(jtStartIndex).Take(jtPageSize).ToList();
        }

        #endregion

        #region Llenar los Combo Box y Opciones de Resultados

        [HttpPost]
        public JsonResult GetEstatusOptions()
        {
            try
            {
                var estatus = getListaEstatus().Select(
                    c => new { DisplayText = c.Nombre, Value = c.EstatusID });
                return Json(new { Result = "OK", Options = estatus });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetTiposOptions()
        {
            try
            {
                var tipos = getListaTipo().Select(
                    c => new { DisplayText = c.Nombre, Value = c.TipoID });
                return Json(new { Result = "OK", Options = tipos });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetAsignadoOptions(int id = 0)
        {
            try
            {
                var tipos = getListaAsignado(0, id).Select(
                    c => new { DisplayText = c.AsignadoANombre, Value = c.AsignadoA });
                return Json(new { Result = "OK", Options = tipos });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        private void SetCombox(bool getEstadoEstatus = false)
        {
            int idModuloConsumo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosConsumo"]);
            var moduloConsumo = BuscarModulo(idModuloConsumo);

            int idModuloHipotecario = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosHipotecarios"]);
            var moduloHipotecario = BuscarModulo(idModuloHipotecario);
            
            bool esSupervisor = (moduloConsumo.permisoEspecial || moduloHipotecario.permisoEspecial);

            listaEstado = unitOfWork.SolicitudesRepository.GetEstadosDropDown(esSupervisor).ToList();
           
            listaTipos = new List<int>();

            if (moduloConsumo.permisoLeer)
            {
                listaTipos.AddRange(unitOfWork.SolicitudesRepository.GetTiposPrestamos((int)ListaAccesos.ModuloConsumoLeer));
            }
            if (moduloHipotecario.permisoLeer)
            {
                listaTipos.AddRange(unitOfWork.SolicitudesRepository.GetTiposPrestamos((int)ListaAccesos.ModuloHipotecarioLeer));
            }
            if (moduloConsumo.permisoEspecial || moduloHipotecario.permisoEspecial)
            {
                ViewBag.Supervisor = "true";
            }

            if (!getEstadoEstatus)
            {
                ViewBag.EmpresaID = new SelectList(getListaEmpresa(), "EmpresaID", "Nombre");
                ViewBag.Asignado = new SelectList(getListaAsignado(), "AsignadoA", "AsignadoANombre");
                ViewBag.EstatusID = new SelectList(getListaEstatus(listaEstado.ToArray()), "EstatusID", "Nombre");
                ViewBag.TipoID = new SelectList(getListaTipo(listaTipos.ToArray()), "TipoID", "Nombre");
            }
        }

        #endregion

        #region Lista y Update de Solicitudes JSON

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult SolicitudesList(int? EmpresaID = 0, int? EstatusID = 0, int? TipoID = 0, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "", string searchString = "", int viewType = 0, int Asignado = 0, string q = "", int qt = 0)
        {
            try
            {
                SetCombox(true);
                //Get data from database
                int solicitudCount = 0;
                var listadoDeSolicitudes = getResultado(jtStartIndex, jtPageSize, jtSorting, searchString, ref solicitudCount, EmpresaID, EstatusID, TipoID, viewType, Asignado, q, qt);
                return Json(new { Result = "OK", Records = listadoDeSolicitudes, TotalRecordCount = solicitudCount, Asignado });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SolicitudUpdate([Bind(Include = "SolicitudID,AsignadoA,AsignadoANombre")] Solicitud modelo)
        {
            try
            {
                if (modelo == null || modelo.AsignadoA == null || modelo.AsignadoA < 1)
                {
                    return Json(new { Result = "ERROR", Message = "Seleccione el Usuario..."});
                }

                Solicitud obj = unitOfWork.SolicitudesRepository.GetByID(modelo.SolicitudID);
                var tipos = getListaAsignado((int)modelo.AsignadoA).FirstOrDefault();
                obj.AsignadoA = tipos.AsignadoA;
                obj.AsignadoANombre = tipos.AsignadoANombre;
                unitOfWork.SolicitudesRepository.Update(obj);
                unitOfWork.Save();

                return Json(new { Result = "OK" });
                
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Lista de Prestamos Actuales JSON

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult PrestamosList(int jtStartIndex = 0, int jtPageSize = 10, string jtSorting = "", string Identificacion = "", int TipoIdentificacion = 0)
        {
            try
            {
				//var listado = wsProd.BuscarPrestamos(Identificacion, TipoIdentificacion, currentUser.usuario, currentUser.ip).Where(p => p.Activo == true);
				//int solicitudCount = listado.Count();

				var listado = new List<Usuario>();
				int solicitudCount = listado.Count();

				//listado = listado.OrderBy(s => s.Numero).Skip(jtStartIndex).Take(jtPageSize).ToArray();

                return Json(new { Result = "OK", Records = listado, TotalRecordCount = solicitudCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Paginas Actions

        public ActionResult Pendientes()
        {
            ViewBag.ViewType = stViewType.Pendientes;
            ViewBag.Title = "Solicitudes Pendientes";
            //ViewBag.Supervisor = 
            SetCombox();
            return View("Index");
        }

        public ActionResult MisSolicitudes()
        {
            ViewBag.ViewType = stViewType.MisSolicitudes;
            ViewBag.Title = "Mis Solicitudes Asignadas";
            SetCombox();
            return View("Index");
        }

        public ActionResult EnProceso()
        {
            ViewBag.ViewType = stViewType.EnProceso;
            ViewBag.Title = "Solicitudes En Proceso";
            SetCombox();
            return View("Index");
        }

        public ActionResult Index()
        {
            ViewBag.ViewType = stViewType.Buscar;
            ViewBag.Title = "Buscar Solicitud";
            SetCombox();
            return View();
        }

        /// <summary>
        /// Procesar la solicitud
        /// </summary>
        /// <param name="id">Solicitud Id</param>
        /// <param name="a">Volver a Cargar la disponibilidad</param>
        /// <returns>Vista creada</returns>
        [HttpGet]
        public ActionResult Procesar(int? id, bool a = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var solicitud = unitOfWork.SolicitudesRepository.GetByID(id);
            
            if (solicitud == null)
            {
                return HttpNotFound();
            }
            
            //var idModulo = getModuloDeLaSolicitud(solicitud.SolicitudID);
        
            var modulo = BuscarModulo(solicitud.Tipos.TipoID);

            if (!modulo.permisoLeer)
            {
                TempData["MensajeError"] = "error|Este usuario No tiene permiso para leer esta solicitud.";
                return RedirectToAction("Pendientes");
            }

            if (!modulo.permisoEditar)
            {
                TempData["MensajeError"] = "error|Este usuario No tiene permiso para editar esta solicitud.";
                return RedirectToAction("Pendientes");
            }

            listaEstadoProceso(solicitud.EstatusID, modulo.permisoEspecial);
            
            EditarSolicitudViewModel modelo = new EditarSolicitudViewModel();

            modelo.solicitud = solicitud;

            modelo.Procesar = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.Procesar).ToList();

            bool existeDisponibilidadCargada = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesIngresos).FirstOrDefault().Existe;
           
            if (existeDisponibilidadCargada && !a)
            {
                modelo.EditarIngresos = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.EditarIngresos).ToList();
                modelo.EditarDeducciones = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.EditarDeducciones).ToList();

                modelo.TotalIngreso = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesIngresos).ToList();
                modelo.TotalDeducciones = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesDeducciones).ToList();
            }
            else
            {
                var objDisponbilidad = unitOfWork.SolicitudesRepository.GetDisponiblidadLista((int)modelo.solicitud.CodigoEmpleado, (int)modelo.solicitud.EmpresaID, modelo.solicitud.SolicitudID);

                modelo.EditarIngresos = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).ToList();
                modelo.EditarDeducciones = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones).OrderBy(p=>p.Nombre).ToList();

                modelo.TotalIngreso = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.TotalesIngresos).ToList();
                modelo.TotalDeducciones = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.TotalesDeducciones).ToList();

                if (a)
                {
                    TempData["MensajeError"] = "ok|Disponibilidad Reiniciada correctamente.";
                }
            }
            
            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Procesar(EditarSolicitudViewModel modelo, string returnUrl)
        {
            string accionVolver = "Pendientes";
            if (modelo == null || modelo.solicitud == null)
            {
                return HttpNotFound();
            }

            Solicitud solicitud = unitOfWork.SolicitudesRepository.GetByID(modelo.solicitud.SolicitudID);

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                int estadoIdInicial = 0;

                //var idModulo = getModuloDeLaSolicitud(solicitud.SolicitudID);

				var modulo = BuscarModulo(solicitud.Tipos.TipoID);

                if (!modulo.permisoLeer)
                {
                    TempData["MensajeError"] = "error|Este usuario No tiene permiso para leer esta solicitud.";
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction(accionVolver);
                    }
                    
                }

                if (!modulo.permisoEditar)
                {
                    TempData["MensajeError"] = "error|Este usuario No tiene permiso para editar esta solicitud.";
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction(accionVolver);
                    }
                }

                if (modelo.solicitud.EstatusID > 0)
                {
                    estadoIdInicial = solicitud.EstatusID;
                    solicitud.EstatusID = modelo.solicitud.EstatusID;
                    solicitud.Estatu = unitOfWork.EstatusRepository.GetByID(modelo.solicitud.EstatusID);
                }

                if (solicitud.AsignadoA != currentUser.codigoEmpleado)
                {
                    solicitud.AsignadoA = currentUser.codigoEmpleado;
                    solicitud.AsignadoANombre = currentUser.nombreCompleto;
                }

                listaEstadoProceso(solicitud.EstatusID, modulo.permisoEspecial);
                
                string strDisponiblidadBruta = "";
                var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Procesar, solicitud, currentUser.codigoEmpleado, currentUser.usuario, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.EditarIngresos, solicitud, currentUser.codigoEmpleado, currentUser.usuario, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.EditarDeducciones, solicitud, currentUser.codigoEmpleado, currentUser.usuario, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.TotalIngreso, solicitud, currentUser.codigoEmpleado, currentUser.usuario, modelo.EditarIngresos, ref strDisponiblidadBruta, 1, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.TotalDeducciones, solicitud, currentUser.codigoEmpleado, currentUser.usuario, modelo.EditarDeducciones, ref strDisponiblidadBruta, 2, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, currentUser.codigoEmpleado, currentUser.usuario, estadoIdInicial);
                unitOfWork.SolicitudesRepository.Update(solicitud);
                
                try
                {
                    unitOfWork.Save();
                    var oficina = BuscarOficina(currentUser.codOficina);
                    string nombreOficina = "";
                    if (oficina != null)
                    {
                        nombreOficina = oficina.Descripcion;
                    }

                    NotificacionCorreo.NotificacionCambioEstado(solicitud, currentUser, estadoIdInicial, nombreOficina);

                    if(estadoIdInicial != solicitud.EstatusID)
                    {
                        TempData["MensajeError"] = "ok|Solicitud Guardada como " + solicitud.Estatu.Nombre + ".";
                    }
                    else
                    {
                        TempData["MensajeError"] = "ok|Solicitud Guardada Correctamente.";
                    }
                    
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                }

                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction(accionVolver);
                }

            }
            else
            {
                foreach (var item in ModelState.Values)
                {
                    var asdfasdf = item;
                    var asdfasdffda = item.Errors;
                    var asdfasdfasdfas = item.Value;
                }
            }

            EditarSolicitudViewModel retVal = new EditarSolicitudViewModel();

            retVal.solicitud = solicitud;

            retVal.EditarIngresos = unitOfWork.SolicitudesRepository.GetPropiedades(retVal.solicitud.SolicitudID, PropiedadesTipos.EditarIngresos).ToList();
            retVal.EditarDeducciones = unitOfWork.SolicitudesRepository.GetPropiedades(retVal.solicitud.SolicitudID, PropiedadesTipos.EditarDeducciones).OrderBy(p=>p.Nombre).ToList();

            retVal.TotalIngreso = unitOfWork.SolicitudesRepository.GetPropiedades(retVal.solicitud.SolicitudID, PropiedadesTipos.TotalesIngresos).ToList();
            retVal.TotalDeducciones = unitOfWork.SolicitudesRepository.GetPropiedades(retVal.solicitud.SolicitudID, PropiedadesTipos.TotalesDeducciones).ToList();
           
            return View(retVal);

        }

        
        private void listaEstadoProceso(int estadoActualId, bool esSupervisor)
        {
            var listaEstado = unitOfWork.SolicitudesRepository.GetEstadosPuedePasar(estadoActualId, esSupervisor).ToArray();
            ViewBag.EstatusID = new SelectList(getListaEstatus(listaEstado), "EstatusID", "Accion");
        }

        public ActionResult Detalle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var solicitud = unitOfWork.SolicitudesRepository.GetByID(id);
            
            if (solicitud == null)
            {
                return HttpNotFound();
            }

            //var idModulo = getModuloDeLaSolicitud(solicitud.SolicitudID);

			var modulo = BuscarModulo(solicitud.Tipos.TipoID);

            if (!modulo.permisoLeer)
            {
                TempData["MensajeError"] = "Este usuario No tiene permiso para ver esta solicitud.";
                return RedirectToAction("Pendientes");
            }

            var modelo = new DetalleSolicitudViewModel();
            modelo.solicitud = solicitud;
            modelo.Detalle = unitOfWork.SolicitudesRepository.GetPropiedades(solicitud.SolicitudID, PropiedadesTipos.Detalle);
            modelo.TotalIngreso = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesIngresos).ToList();
            modelo.TotalDeducciones = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesDeducciones).ToList();
            modelo.Procesar = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.Procesar).ToList();
            modelo.ExisteDisponibilidadCargada = unitOfWork.SolicitudesRepository.GetPropiedades(modelo.solicitud.SolicitudID, PropiedadesTipos.TotalesIngresos).FirstOrDefault().Existe;
                        
            bool esSupervisor = modulo.permisoEspecial;

            // Consultar estados que puede colocar a la solicitud.

            return View(modelo);
        }
        
        #endregion
    }
}
