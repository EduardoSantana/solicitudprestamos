﻿using AutenticacionData.Data;
using SeguridadComun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolicitudEmpleado.MVC.Controllers
{
	public class SeguridadAppV2
	{
		private int _idUsuario { get; set; }
		public SeguridadAppV2(int idUsuario) {
			_idUsuario = idUsuario;
		}
		//
		// Summary:
		//     Grupo por defecto al que sera logueado el usuario en caso de asi requerirlo
		public int? grupoDefecto { get; set; }
		//
		// Summary:
		//     Identificacion de la aplicacion en la base de datos
		public int idAplicacion { get; set; }
		//
		// Summary:
		//     Id del grupo al que pertenece para esa aplicacion
		public int idGrupo { get; set; }
		//
		// Summary:
		//     Nombre de la aplicacion en la base de datos
		public string nombreAplicacion { get; set; }
		//
		// Summary:
		//     Nombre del grupo
		public string nombreGrupo { get; set; }
		//
		// Summary:
		//     La informacion completa del usuario
		public Usuario usuario { get; set; }
		//
		// Summary:
		//     Valor que indica si el usuario se encuentra banneado
		public bool? aceptado { get; set; }
		//
		// Summary:
		//     Valor que contiene si el usuario es administrador o no
		public bool? admin { get; set; }
		//
		// Summary:
		//     Un string para conectarse a la base de datos
		protected string connstr { get; set; }
		//
		// Summary:
		//     Valor que indica que ya hemos verificado todos los aspectos de la seguridad para
		//     determinar si el usuario tiene permisos
		protected bool? postCheck { get; set; }
		//
		// Summary:
		//     Url del web service de Recursos Humanos
		protected string webRRHH { get; set; }
		//
		// Summary:
		//     El URL del WebService
		protected string webs { get; set; }
		
		//
		// Summary:
		//     Valor que indica si el usuario actual se encuentra excluido del sistema por alguna
		//     de las reglas enviadas
		public bool CumpleReglas { get; set; }
		
		//
		// Summary:
		//     Indica si el usuario conectado es administrador en la aplicacion
		public bool EsAdministrador { get; set; }
		
		//
		// Summary:
		//     Lista de objetos SeguridadMod que representa la seguridad para cada modulo para
		//     esta instancia.
		public List<SeguridadMod> Modulos { get; set; }
		//
		// Summary:
		//     Indica si el usuario tiene o no acceso a la aplicacion segun la seguridad
		public bool UsuarioAccesa { get; set; }

		private List<Accesos> listaAccesos { get; set; }

		public bool TieneAcceso(int idAcceso)
		{

			if (listaAccesos == null)
			{
				var db = new dbAutenticacion();
				listaAccesos = (from p in db.Usuarios from p2 in p.Roles from p3 in p2.Accesos where p.idUsuario == _idUsuario select p3).ToList();
			}

			return listaAccesos.Where(p => p.idAcceso == idAcceso).Any();
		}

		//
		// Summary:
		//     Metodo que busca un modulo por su numero de ID y lo retorna si lo encuentra,
		//     si no retorna el modulo vacio
		//
		// Parameters:
		//   id:
		//     Id del modulo a buscar
		//
		// Returns:
		//     El modulo encontrado por el metodo
		public SeguridadMod BuscarModulo(int idModulo)
		{
			var retVal = new SeguridadMod();
			string permisos = "rweds";
			string nombreModulo = "";
			int idModuloConsumo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosConsumo"]);
			int idModuloHipotecario = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosHipotecarios"]);

			if (idModulo == idModuloConsumo)
			{

				nombreModulo = "PrestamosConsumo";

				if (!TieneAcceso((int)ListaAccesos.ModuloConsumoEditar))
				{
					permisos = permisos.Replace("e", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloConsumoEliminar))
				{
					permisos = permisos.Replace("d", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloConsumoEscribir))
				{
					permisos = permisos.Replace("w", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloConsumoLeer))
				{
					permisos = permisos.Replace("r", "-");
				}

			}

			if (idModulo == idModuloHipotecario)
			{

				nombreModulo = "PrestamosHipotecarios";

				if (!TieneAcceso((int)ListaAccesos.ModuloHipotecarioEditar))
				{
					permisos = permisos.Replace("e", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloHipotecarioEliminar))
				{
					permisos = permisos.Replace("d", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloHipotecarioEscribir))
				{
					permisos = permisos.Replace("w", "-");
				}
				if (!TieneAcceso((int)ListaAccesos.ModuloHipotecarioLeer))
				{
					permisos = permisos.Replace("r", "-");
				}

			}

			return new SeguridadMod(idModulo, nombreModulo, permisos, true);

		}

	}
}