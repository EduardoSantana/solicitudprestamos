﻿using SeguridadComun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutenticacionData.Data;

namespace SolicitudEmpleado.MVC.Controllers
{
	public class BaseSeguridadController : Controller
	{
		private  SeguridadAppV2 _seguridadApp { get; set; }
		private  SeguridadComun.Usuario _CurrentUser { get; set; }
		private  SeguridadComun.Usuario convertirUsuario(AutenticacionData.Data.Usuarios usuario)
		{
			var retVal = new SeguridadComun.Usuario()
			{
				idUsuario = usuario.idUsuario,
				activo = usuario.activo,
				apellido = usuario.apellido,
				cedula = Int64.Parse(usuario.cedula),
				cedulaForm = usuario.cedulaForm,
				celular = Int64.Parse(usuario.celular),
				celularForm = usuario.celularForm,
				codigoEmpleado = (usuario.codigoEmpleado != null) ? int.Parse(usuario.codigoEmpleado) : 0,
				codOficina = usuario.codOficina,
				correo = usuario.correo,
				departamento = usuario.departamento,
				empresa = ((usuario.empresa != null) ? (int)usuario.empresa : 1),
				ext = ((usuario.ext != null) ? (int)usuario.ext : 0),
				ip = usuario.ip,
				navegador = usuario.navegador,
				nombre = usuario.nombre,
				nomOficina = usuario.nomOficina,
				puesto = usuario.puesto,
				telefono = Int64.Parse(usuario.telefono),
				telefonoForm = usuario.telefonoForm,
				usuario = usuario.usuario,

			};
			return retVal;
		}

		public  bool TieneAcceso(int idAcceso)
		{
			return ObtenerSeguridad().TieneAcceso(idAcceso);
		}

		public SeguridadMod BuscarModulo(int idModulo)
		{
			return ObtenerSeguridad().BuscarModulo(idModulo);
		}

		public Usuario BuscarUsuario(int idUsuario)
		{
			var db = new dbAutenticacion();
			var usuario = db.Usuarios.Find(idUsuario);
			if (usuario != null)
			{
				return convertirUsuario(usuario);
			}

			return new Usuario();
		}

		public  Usuario ObtenerUsuario()
		{
			if (_CurrentUser == null)
			{
				string userName = User.Identity.Name; 
				var db = new dbAutenticacion();
				var tempUser = db.Usuarios.Where(p => p.usuario == userName).FirstOrDefault();
				if (tempUser != null)
				{
					_CurrentUser = convertirUsuario(tempUser);
				}
				else
				{
					_CurrentUser = new Usuario(userName);
				}
			}

			return _CurrentUser;

		}

		public  SeguridadAppV2 ObtenerSeguridad()
		{
			if (_seguridadApp == null)
			{
				_seguridadApp = new SeguridadAppV2(ObtenerUsuario().idUsuario);
				_seguridadApp.idAplicacion = 53;
				_seguridadApp.idGrupo = 0;
				_seguridadApp.nombreAplicacion = "NombreAplicacion";
				_seguridadApp.usuario = ObtenerUsuario();
				_seguridadApp.aceptado = true;
				_seguridadApp.admin = true;
				_seguridadApp.CumpleReglas = true;
				_seguridadApp.EsAdministrador = true;
				_seguridadApp.Modulos = new List<SeguridadMod>();
				_seguridadApp.Modulos.Add(BuscarModulo(218));
				_seguridadApp.Modulos.Add(BuscarModulo(223));
				_seguridadApp.Modulos.Add(BuscarModulo(224));
				_seguridadApp.UsuarioAccesa = true;

			}
			return _seguridadApp;
		}
	}

}