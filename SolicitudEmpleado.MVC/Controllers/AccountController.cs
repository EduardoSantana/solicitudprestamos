﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web.Security;
using System.Configuration;
using System.Net;
using AutenticacionData.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleado.MVC.Controllers
{
	
	public class AccountController : Controller
	{
		private dbAutenticacion db = new dbAutenticacion();

		[AllowAnonymous]
		public ActionResult Login(LoginViewModel model, string returnUrl)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(model);
			}
			bool validated = false;

			bool dataBaseValidated = true; // Convert.ToBoolean(ConfigurationManager.AppSettings["dataBaseValidated"]);

			if (dataBaseValidated)
			{
				validated = model.IsValid();
			}
			else
			{
				validated = ValidarWindowsAuthentication(model);
			}

			if (validated)
			{
				FormsAuthentication.SetAuthCookie(model.UserName, false);
				LoggedIn(model.UserName);
				//if (this.Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
				//	&& !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
				//{
				//	return this.Redirect(returnUrl);
				//}
				return this.RedirectToAction("Index", "Inicio");
			}
			else
			{
				this.ModelState.AddModelError(string.Empty, "The user name or password provided is incorrect.");
			}
			return this.View(model);
		}

		private void LoggedIn(string userName)
		{
			
		}

		private bool ValidarWindowsAuthentication(LoginViewModel model)
		{
			bool validated = false;
			if (Membership.ValidateUser(model.UserName, model.Password))
			{
				var usuarioActivo = (from data in db.Usuarios
									 where data.usuario == model.UserName
									 select data.activo).FirstOrDefault();

				validated = usuarioActivo;
				
				if (!usuarioActivo)
				{
					this.ModelState.AddModelError(string.Empty, "Usuario Inactivo");
				}
			}

			return validated;
		}

	
		//
		// POST: /Account/LogOff
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			FormsAuthentication.SignOut();
			return this.RedirectToAction("Login", "Account");
		}

	
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		#region Helpers

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		public enum ManageMessageId
		{
			ChangePasswordSuccess,
			SetPasswordSuccess,
			RemoveLoginSuccess,
			Error
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Index", "Home");
			}
		}

		#endregion
	}
}