﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using SolicitudEmpleado.MVC.WebReferences.Oficinas;
using SeguridadComun;

namespace SolicitudEmpleado.MVC.Controllers
{
    public class SeguridadController : BaseSeguridadController
	{
        //
        // GET: /Seguridad/Usuario-Conectado

        [ActionName("Usuario-Conectado")]
        public ActionResult UsuarioConectado()
        {
            //var seguridad = ObtenerSeguridad();
            var usuario = ObtenerUsuario();

			//var oficina = BuscarOficina(usuario.codOficina);

   //         if (oficina != null)
   //             ViewBag.Oficina = oficina.Descripcion;

            ViewBag.OpcionesAvanzadas = true;
			ViewBag.GrupoUsuario = "NombreGrupo";

            if (Request.IsAjaxRequest())
                return PartialView(usuario);

            return View(usuario);
        }

        //
        // GET: /Seguridad/Usuario

        public ActionResult Usuario(int id)
        {
            var usuario = BuscarUsuario(id);

            if (usuario == null)
                return HttpNotFound();

            var oficina = BuscarOficina(usuario.codOficina);

            if (oficina != null)
                ViewBag.Oficina = oficina.Descripcion;

            if (Request.IsAjaxRequest())
                return PartialView(usuario);

            return View(usuario);
        }

        //
        // GET: /Seguridad/Refrescar

        //public ActionResult Refrescar()
        //{
        //    RefrescarSeguridad();

        //    return RedirectToAction("Usuario-Conectado");
        //}

        //
        // GET: /Seguridad/Oficina

        public ActionResult Oficina(string id)
        {
            var oficina = BuscarOficina(id);

            if (oficina == null)
                return HttpNotFound();

            return View(oficina);
        }

        private Oficina BuscarOficina(string codigo)
        {
            string serviceUrl = WebConfigurationManager.AppSettings["Seguridad.Url.Oficinas"];
            var service = new OficinasService(serviceUrl);

            var request = new OficinaRequest()
            {
                Info = new ServiceRequestInfo()
                {
                    Aplicacion = WebConfigurationManager.AppSettings["Seguridad.App.Nombre"],
                    IP = Request.UserHostAddress,
                    Usuario = User.Identity.Name
                },
                Codigo = codigo
            };

            var response = service.BuscarOficina(request);
            return response.Oficina;
        }
    }
}