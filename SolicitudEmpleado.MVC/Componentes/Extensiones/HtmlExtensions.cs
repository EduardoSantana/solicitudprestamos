﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml.Linq;
//using IntranetComun.Mvc4;
using SolicitudEmpleado.MVC.Componentes.Extensiones.Menu;
using SeguridadComun;


namespace SolicitudEmpleado.MVC.Componentes.Extensiones
{
	public static class ViewContextExtension
	{
		public static Controllers.BaseSeguridadController BaseController(this ViewContext view)
		{
			Controllers.BaseSeguridadController baseController = (Controllers.BaseSeguridadController)view.Controller;
			return baseController;
		}
	}

	public static class HtmlExtensions
    {
		#region Menu

		
		public static IHtmlString MenuAplicacion(this HtmlHelper html, string rutaMenu, Controllers.SeguridadAppV2 seguridad = null)
    {
			if (seguridad == null)
				seguridad = html.ViewContext.BaseController().ObtenerSeguridad();


			var xml = XElement.Load(Context.Server.MapPath(rutaMenu));
            FiltrarElementosPermiso(xml, seguridad);

            var result = new StringWriter();
            using (var writer = new HtmlTextWriter(result))
            {
                ImprimirOpciones(html.ViewContext, writer, xml.Elements("MenuItem"), true);
            }

            return new HtmlString(result.ToString());
        }

        private static void FiltrarElementosPermiso(XElement xml, Controllers.SeguridadAppV2 seguridad)
        {
            bool hadElements;
            var elements = xml.Elements("MenuItem").ToList();
            foreach (var element in elements)
            {
                hadElements = element.HasElements;
                if (hadElements)
                    FiltrarElementosPermiso(element, seguridad);

                if (hadElements && !element.HasElements)
                {
                    element.Remove();
                    continue;
                }

                var nodoModulo = element.Attribute("Modulo");
                var nodoPermisos = element.Attribute("Permisos");
                if (!ValidarSeguridadMenu(nodoModulo, nodoPermisos, seguridad))
                    element.Remove();
            }
        }

        private static void ImprimirOpciones(ViewContext context, HtmlTextWriter writer, IEnumerable<XElement> elementos, bool first = false)
        {
            if (!first)
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu");
            else
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            foreach (var elemento in elementos)
            {
                var nodoHref = elemento.Attribute("Href");
                var nodoIcon = elemento.Attribute("Icono");
                var nodoProcesar = elemento.Attribute("Procesar");
                var nodoClase = elemento.Attribute("Clase");

                if (elemento.HasElements)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown");
                else if (nodoClase != null)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, nodoClase.Value);

                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                if (elemento.HasElements)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-toggle");
                    writer.AddAttribute("data-toggle", "dropdown");
                }

                if (nodoHref == null || String.IsNullOrEmpty(nodoHref.Value))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, ObtenerHref(nodoHref.Value));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                }

                if (nodoIcon != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, nodoIcon.Value);
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.RenderEndTag();
                    writer.Write("&nbsp;");
                }

                string valor = elemento.Attribute("Nombre").Value;

                if (nodoProcesar != null && String.Equals(nodoProcesar.Value, Boolean.TrueString))
                    valor = ProcesarContenidoTexto(valor, context);

                writer.Write(valor);
                writer.RenderEndTag();

                if (elemento.HasElements)
                    ImprimirOpciones(context, writer, elemento.Descendants());

                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        private static bool ValidarSeguridadMenu(XAttribute nodoModulo, XAttribute nodoPermisos, Controllers.SeguridadAppV2 seguridad)
        {
            if (nodoModulo == null || nodoPermisos == null || nodoModulo.Value == null || nodoPermisos.Value == null)
                return true;

            var modulo = ObtenerModulo(nodoModulo.Value, seguridad);
            if (modulo != null)
            {
                string permisos = nodoPermisos.Value;
                for (int i = 0; i < permisos.Length; i++)
                {
                    if (permisos[i] != '-')
                    {
                        // Si no tenemos el permiso de indice i en "rweds", no mostramos
                        if (modulo[(TiposPermisos)i] == false)
                            return false;
                    }
                }
            }

            return true;
        }

        private static string ProcesarContenidoTexto(string valor, ViewContext context)
        {
            return ElementosProcesados.ProcesarContenido(valor, context);
        }

        #endregion

        #region Comun

        private static HttpContext Context = HttpContext.Current;

        private static bool ValidarSeguridadModulo(string nombre, Controllers.SeguridadAppV2 seguridad)
        {
            var modulo = ObtenerModulo(nombre, seguridad);
            return modulo.permisoLeer;
        }

        private static SeguridadMod ObtenerModulo(string nombre, Controllers.SeguridadAppV2 seguridad)
        {
			//string llave = SeguridadConstantes.Prefijo + "." + nombre;
			string llave = "Seguridad" + "." + nombre;
			string idModulo = WebConfigurationManager.AppSettings[llave];

			if (String.IsNullOrEmpty(idModulo))
				throw new ArgumentException("El modulo " + llave + " no esta registrado");

			int id = Int32.Parse(idModulo);
            var modulo = seguridad.BuscarModulo(id);
            return modulo;
        }

        private static string ObtenerHref(string ruta)
        {
            if (String.Equals(ruta, "#"))
                return ruta;

            if (Uri.IsWellFormedUriString(ruta, UriKind.Absolute))
                return ruta;

            return VirtualPathUtility.ToAbsolute(ruta).ToLower();
        }

        #endregion
    }
}