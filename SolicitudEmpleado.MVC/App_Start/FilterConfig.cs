﻿using System.Web;
using System.Web.Mvc;
//using IntranetComun.Mvc4;

namespace SolicitudEmpleado.MVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new ValidarAplicacionAttribute());
        }
    }
}