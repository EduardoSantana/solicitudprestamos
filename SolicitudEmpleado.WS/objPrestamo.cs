﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.WS
{
    public class objPrestamo
    {
        public string NumeroFormato { get; set; }
        public string ValorCuota { get; set; }
        public string Descripcion { get; set; }
        public string MontoAprobado { get; set; }
        public string TotalAdeudado { get; set; }
        public string PorcientoPagado { get; set; }
        public bool Activo { get; set; }
        public string Numero { get; set; }
        public DateTime FechaDesembolso { get; set; }
    }
}
