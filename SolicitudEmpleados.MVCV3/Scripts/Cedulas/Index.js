﻿$(document).ready(function () {
    $("#btnBuscar").click(function (e) {
        e.preventDefault();
        var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
        var urlPath = 'Cedulas/Index/' + $("#nombreCedula").val().trim().toUpperCase() + "?q=" + $("#apellido").val().trim().toUpperCase();
        Redirect(divRoot + urlPath);
    });
});