﻿$(document).ready(function () {
    processServerJS();
    manageNavigation();
    $(document).on('blur', "input[type=text]", function () {
        $(this).val(function (_, val) {
            return val.toUpperCase();
        });
    });
});

function processServerJS() {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    $('*[data-validacionjs="true"]').each(function (index, element) {
        // Test App 
        // console.log('element at index ' + index + 'is ' + (this.name));
        // console.log('current element as dom object:' + element);
        // console.log('current element as jQuery object:' + $(this));
        // End Test App 
        $(this).on('change', function () {
            $.ajax({
                url: url + 'api/ValidacionJSV3/' + this.getAttribute('data-propiedadid') + '?q=' + this.value.trim(),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.result = "ok") {
                        $.each(data.lista, function (index, value) {
                            // Test App 
                            // console.log('element at index ' + index + ' is ' + value);
                            // End Test App 
                            if (!isEmpty(value.value)) {
                                $(value.name).val(value.value);
                            }
                           
                        });
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

function manageNavigation() {

    function moveNavigationTo(index) {
        $(".panels > div").removeClass("active");
        $(".panels > .panel-" + index).addClass("active");
        $("ul.breadcrumbs li").removeClass("active");
        $("ul.breadcrumbs li.li-" + index).addClass("active");
        if (index == 1) {
            $(".back").addClass("hide");
        } else {
            $(".back").removeClass("hide");
        }
    }
    $(document).on('click', ".item-li", function () {
        var index = parseInt($(this).data('index'));
        moveNavigationTo(index);
    });
    $(document).on('click', ".next", function () {
        var index = parseInt($(".panels > .active").data('index')) + 1;
        if ($(".panels > .panel-" + index).length > 0) {
            moveNavigationTo(index);
        }
        else {
            $(".submit-form").click();
        }
    });
    $(document).on('click', ".back", function () {
        var index = parseInt($(".panels > .active").data('index')) - 1;
        moveNavigationTo(index);
    });
}