﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.PadronData;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class CedulasController : BaseController
    {
        private dbDatosDominicanos db = new dbDatosDominicanos();

        // GET: CEDULAS
        public ActionResult Index(string id = "", string q = "")
        {
			var retVal = new List<CEDULAS>();
			if (String.IsNullOrEmpty(id) && String.IsNullOrEmpty(q))
			{ 
				return View(db.ConsultaCedula());
			}
			else
			{
				long buscarNumero = 0;
				if (long.TryParse(id, out buscarNumero)) 
				{
					retVal = db.ConsultaCedula(buscarNumero, buscarNumero);
				}
				else
				{
					retVal = db.ConsultaCedula(null, null, id, q);
				}

			}
			ViewBag.id = id;
			ViewBag.q = q;
			return View(retVal);
        }

        // GET: CEDULAS/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			CEDULAS Cedula = db.ConsultaCedula(id).FirstOrDefault(); 
            if (Cedula == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(Cedula);

            return View(Cedula);
        }

		//// GET: CEDULAS/Create
		//public ActionResult Create()
		//{
		//	if (Request.IsAjaxRequest())
		//			return PartialView();

		//	return View();
		//}

		//// POST: CEDULAS/Create
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Create([Bind(Include = "ID_CEDULAS,CEDULA_NUEVA,MUN_CED,SEQ_CED,VER_CED,CEDULA_ANTERIOR,CED_A_NUM,CED_A_SERI,SEXO_ANTERIOR,NOMBRES,APELLIDOS,FECHA_NAC,LUGAR_NAC,SEXO_ACTUAL,DESC_SEXO,COD_SANGRE,DESC_SANGRE,COD_PIEL,DESC_PIEL,ESTADO_CIVIL_NUEVO,DESC_ESTADO_CIVIL_NUEVO,SENAS_PARTICULARES,COD_OCUP,OCUPACION,PROVINCIA,COD_MUNICIPIO,DESC_MUNICIPIO,CODIGO_CIUDAD,DESC_CIUDAD,CODIGO_SECTOR,SECTOR,CALLE,NUMERO,EDIFICIO,PISO,APARTAMENTO,TELEFONO,NOMBRE_PADRE,CEDULA_PADRE,NOMBRE_MADRE,CEDULA_MADRE,NOMBRE_CONYUGUE,CED_CONYUGUE,COD_NACION,DESC_NACIONALIDAD,NO_PASAPORTE,NO_RESIDENCIA,FECHA_EXPEDICION,FECHA_NATURALIZACION,COD_CATEGORIA,DESC_CATEGORIA,TIPO_CAUSA,DESC_TIPO_CAUSA,COD_CAUSA,DESC_CAUSA,STATUS,DIRECTORIO,ARCHIVO,SECUENCIA,FECHA_CREO,USUARIO_CREO")] CEDULAS cEDULAS)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		cEDULAS.AuditoriaCrear(currentUser.codigoEmpleado);
		//		db.CEDULAS.Add(cEDULAS);
               
		//		try
		//		{
		//			db.SaveChanges();
		//		}
		//		catch (System.Data.Entity.Validation.DbEntityValidationException ex)
		//		{
                    
		//			ModelState.AddModelError("", "No se ha podido guardar CEDULAS. Por los siguientes errores: ");
		//			var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
		//			foreach (var item in listaValidaciones)
		//			{
		//				foreach (var itemInside in item.ValidationErrors)
		//				{
		//					ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
		//				}
		//			}

		//			return View(cEDULAS);
		//		}
		//		catch (Exception ex)
		//		{
		//			ModelState.AddModelError("", "No se ha podido guardar CEDULAS. Por los siguientes errores: ");
		//			ModelState.AddModelError("", ex.Message);
		//			if (ex.InnerException != null)
		//			{
		//				string mensajeErrorInner = ex.InnerException.ToString();
		//				if (mensajeErrorInner.Length > 500)
		//				{
		//					mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
		//				}
		//				ModelState.AddModelError("", mensajeErrorInner);
		//			}
                    
		//			return View(cEDULAS);
		//		}

		//		if (Request.IsAjaxRequest())
		//			return PartialView(cEDULAS);

		//		return RedirectToAction("Index");
		//	}

		//	if (Request.IsAjaxRequest())
		//		return PartialView(cEDULAS);

		//	return View(cEDULAS);
		//}

		//// GET: CEDULAS/Edit/5
		//public ActionResult Edit(long? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	CEDULAS cEDULAS = db.CEDULAS.Find(id);
		//	if (cEDULAS == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	if (Request.IsAjaxRequest())
		//		return PartialView(cEDULAS);

		//	return View(cEDULAS);
		//}

		//// POST: CEDULAS/Edit/5
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit([Bind(Include = "ID_CEDULAS,CEDULA_NUEVA,MUN_CED,SEQ_CED,VER_CED,CEDULA_ANTERIOR,CED_A_NUM,CED_A_SERI,SEXO_ANTERIOR,NOMBRES,APELLIDOS,FECHA_NAC,LUGAR_NAC,SEXO_ACTUAL,DESC_SEXO,COD_SANGRE,DESC_SANGRE,COD_PIEL,DESC_PIEL,ESTADO_CIVIL_NUEVO,DESC_ESTADO_CIVIL_NUEVO,SENAS_PARTICULARES,COD_OCUP,OCUPACION,PROVINCIA,COD_MUNICIPIO,DESC_MUNICIPIO,CODIGO_CIUDAD,DESC_CIUDAD,CODIGO_SECTOR,SECTOR,CALLE,NUMERO,EDIFICIO,PISO,APARTAMENTO,TELEFONO,NOMBRE_PADRE,CEDULA_PADRE,NOMBRE_MADRE,CEDULA_MADRE,NOMBRE_CONYUGUE,CED_CONYUGUE,COD_NACION,DESC_NACIONALIDAD,NO_PASAPORTE,NO_RESIDENCIA,FECHA_EXPEDICION,FECHA_NATURALIZACION,COD_CATEGORIA,DESC_CATEGORIA,TIPO_CAUSA,DESC_TIPO_CAUSA,COD_CAUSA,DESC_CAUSA,STATUS,DIRECTORIO,ARCHIVO,SECUENCIA,FECHA_CREO,USUARIO_CREO")] CEDULAS cEDULAS)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		cEDULAS.AuditoriaEditar(currentUser.codigoEmpleado);
		//		db.Entry(cEDULAS).State = EntityState.Modified;
		//		db.SaveChanges();
		//		if (Request.IsAjaxRequest())
		//			return PartialView();
		//		return RedirectToAction("Index");
		//	}
		//	if (Request.IsAjaxRequest())
		//			return PartialView(cEDULAS);

		//	return View(cEDULAS);
		//}

		//// GET: CEDULAS/Delete/5
		//public ActionResult Delete(long? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	CEDULAS cEDULAS = db.CEDULAS.Find(id);
		//	if (cEDULAS == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	if (Request.IsAjaxRequest())
		//		return PartialView(cEDULAS);

		//	return View(cEDULAS);
		//}

		//// POST: CEDULAS/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed(long id)
		//{
		//	CEDULAS cEDULAS = db.CEDULAS.Find(id);
		//	db.CEDULAS.Remove(cEDULAS);
            
		//		try
		//		{
		//			db.SaveChanges();
		//		}
		//		catch (Exception ex)
		//		{
		//			ModelState.AddModelError("", "No se ha podido Eliminar CEDULAS. Por los siguientes errores: ");
		//			ModelState.AddModelError("", ex.Message);
		//			if (ex.InnerException != null)
		//			{
		//				string mensajeErrorInner = ex.InnerException.ToString();
		//				if (mensajeErrorInner.Length > 500)
		//				{
		//					mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
		//				}
		//				ModelState.AddModelError("", mensajeErrorInner);
		//			}
                    
		//			return View(cEDULAS);
		//		}


		//	if (Request.IsAjaxRequest())
		//		return PartialView();
		//	return RedirectToAction("Index");
		//}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
