﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SolicitudEmpleado.PadronData;

namespace SolicitudEmpleados.MVCV3.Controllers.Api
{
	[Authorize]
    public class CedulasController : ApiController
    {
        private dbDatosDominicanos db = new dbDatosDominicanos();

        // GET: api/Cedulas/5
        [ResponseType(typeof(CEDULAS))]
        public IHttpActionResult GetCEDULAS(long id)
        {
			CEDULAS cedula = db.ConsultaCedula(id).FirstOrDefault(); 
            if (cedula == null)
            {
                return NotFound();
            }

            return Ok(cedula);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}