﻿using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.PadronData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SolicitudEmpleados.MVCV3.Controllers.Api
{
	public class retvalidacionjs
	{
		public retvalidacionjs()
		{
			result = "ok";
			lista = new List<retitem>();
		}
		public string message { get; set; }
		public string result { get; set; }
		public List<retitem> lista { get; set; }
	}
	public class retitem
	{
		public string name { get; set; }
		public string value { get; set; }
	}
	public class ValidacionJSController : ApiController
	{
		private dbSolicitudesContext dbSolicitud = new dbSolicitudesContext();

		[ResponseType(typeof(retvalidacionjs))]
		// POST: api/ValidacionJS
		public IHttpActionResult Post(int id, decimal q, int InvitacionId)
		{
			var retVal = new retvalidacionjs();
			var invitacion = dbSolicitud.Invitaciones.Where(p => p.InvitacionId == InvitacionId && p.Token != null).FirstOrDefault();
			if (invitacion != null)
			{
				if (id == CodigoPropiedad.Cedula)
				{
                    var db = new dbDatosDominicanos();
                    var cedula = db.ConsultaCedula(q).FirstOrDefault();
					if (cedula != null)
					{
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Nombre.ToString()), value = (string.IsNullOrEmpty(cedula.NOMBRES) ? "" : cedula.NOMBRES.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Apellido.ToString()), value = (string.IsNullOrEmpty(cedula.APELLIDOS) ? "" : cedula.APELLIDOS.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Sexo.ToString()), value = (string.IsNullOrEmpty(cedula.SEXO_ACTUAL) ? "" : cedula.SEXO_ACTUAL.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.TipoDeSangre.ToString()), value = (string.IsNullOrEmpty(cedula.DESC_SANGRE) ? "" : cedula.DESC_SANGRE.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.EstadoCivil.ToString()), value = (string.IsNullOrEmpty(cedula.DESC_ESTADO_CIVIL_NUEVO) ? "" : cedula.DESC_ESTADO_CIVIL_NUEVO.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Telefono.ToString()), value = (string.IsNullOrEmpty(cedula.TELEFONO) ? "" : cedula.TELEFONO.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.FechaNacimiento.ToString()), value = (string.IsNullOrEmpty(cedula.FECHA_NAC) ? "" : cedula.FECHA_NAC.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.LugarNacimiento.ToString()), value = (string.IsNullOrEmpty(cedula.LUGAR_NAC) ? "" : cedula.LUGAR_NAC.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Nacionalidad.ToString()), value = (string.IsNullOrEmpty(cedula.DESC_NACIONALIDAD) ? "" : cedula.DESC_NACIONALIDAD.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Direccion.ToString()), value = (string.IsNullOrEmpty(cedula.DIRECCION) ? "" : cedula.DIRECCION.Trim()) });
						retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.Correo.ToString()), value = (string.IsNullOrEmpty(invitacion.Correo) ? "" : invitacion.Correo.Trim()) });
					}
				}
			}
			return Ok(retVal);
		}
	}
}
