﻿using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.PadronData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SolicitudEmpleados.MVCV3.Controllers.Api
{
	public class ValidacionJSV3Controller : ApiController
	{
		private dbSolicitudesContext dbSolicitud = new dbSolicitudesContext();

		[ResponseType(typeof(retvalidacionjs))]
		// POST: api/ValidacionJS
		public IHttpActionResult Post(int id, decimal q)
		{
            var retVal = new retvalidacionjs();
            
            if (id == CodigoPropiedad.DetalledePlanPlan)
			{
                // buscar idPlan
                retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanCantidad), value = "1" });
                retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanMinutos), value = "400" });
                retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaUnitaria), value = "500" });
                retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaNeta), value = (1*500).ToString() });
            }
			return Ok(retVal);
		}
	}
}
